FROM jimmycuadra/rust:1.15.0

ENV CARGO_HOME target/cargo_cache

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -yqq && \
    apt-get install -yqq libssl-dev libsqlite3-dev && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["cargo", "build"]
