#[macro_use]
extern crate log;
#[macro_use]
extern crate iron;
extern crate params;
extern crate router;
extern crate mount;
extern crate logger;
extern crate r2d2;
extern crate r2d2_sqlite;
extern crate rusqlite;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate persistent;
extern crate pretty_env_logger;

mod server;
mod api;
mod image_redirect;

fn main() {
    use std::env;
    use r2d2_sqlite::SqliteConnectionManager;

    // setup logging
    pretty_env_logger::init().unwrap();

    // setup database
    let config = r2d2::Config::default();
    let manager = SqliteConnectionManager::new("faces.db");
    let pool = r2d2::Pool::new(config, manager).unwrap();

    // get interface & port
    let interface = env::var("INTERFACE").unwrap_or("localhost".to_owned());
    let port = env::var("PORT").unwrap_or("8370".to_owned()).parse::<u16>().unwrap();
    println!("Starting Server on http://{}:{}", interface, port);

    // run server
    server::run((interface.as_str(), port), pool);
}
