pub mod images;

use mount::Mount;

pub fn build_router() -> Mount {
    debug!("building api router");
    let mut mount = Mount::new();
    mount.mount("/images", images::build_router());

    mount
}