use iron::prelude::*;
use router::Router;
use persistent::Read;

use iron::{headers, status};
use iron::modifiers::Header;

use serde_json;

use server::DbPool;

#[derive(Serialize, Deserialize, Debug)]
struct ImageData {
    id: String,
    srcid: String,
    artistname: String,
    artistusername: String,
    created: String,
}

fn root_handler(req: &mut Request) -> IronResult<Response> {
    use params::Params;

    // grab database connection
    let pool = req.get::<Read<DbPool>>().expect("couldn't find database pool handle").clone();
    let conn = pool.get().expect("couldn't get db conn");

    let params = req.get_ref::<Params>()
        .expect("couldn't get params")
        .to_strict_map::<String>()
        .expect("couldn't parse params as string");

    let es = "".to_owned();

    let count_default = 10;
    let count = match params.get("count").unwrap_or(&es).parse::<u64>() {
        Ok(count) => count,
        Err(_) => count_default,
    };

    let offset_default = 0;
    let offset = match params.get("page").unwrap_or(&es).parse::<u64>() {
        Ok(page) => page * count,
        Err(_) => offset_default,
    };

    let count_str = format!("{}", count);
    let offset_str = format!("{}", offset);

    let mut stmt =
        conn.prepare_cached("SELECT id, srcid, artistname, artistusername, created FROM faces \
                             WHERE id NOT IN ( SELECT faceId FROM facestagsmap JOIN tags ON \
                             tags.tagId = facestagsmap.tagId WHERE tags.tag = 'bad') ORDER BY \
                             created DESC LIMIT ?1, ?2")
            .unwrap();

    let users_iter = stmt.query_map(&[&offset_str, &count_str], |row| {
            ImageData {
                id: row.get(0),
                srcid: row.get(1),
                artistname: row.get(2),
                artistusername: row.get(3),
                created: row.get(4),
            }
        })
        .expect("query_map get_user_by_service_phone");

    let mut users = Vec::new();
    for user in users_iter {
        users.push(user.unwrap());
    }

    let content = serde_json::to_string(&users).unwrap();

    Ok(Response::with((status::Ok, Header(headers::ContentType::json()), content)))
}

pub fn build_router() -> Router {
    let mut router = Router::new();
    router.get("/", root_handler, "root");

    // twilio handlers
    // router.post("/new_call", new_call_handler, "new call");
    // router.post("/check_passcode", check_password_handler, "check passcode");

    router
}
