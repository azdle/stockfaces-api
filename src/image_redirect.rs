use iron::prelude::*;
use router::Router;
use persistent::Read;

use iron::{headers, status};
use iron::modifiers::Header;

use server::DbPool;

use rusqlite::Connection;
use rusqlite;

fn get_image_url(conn: &Connection, id: &str) -> Result<Option<String>, rusqlite::Error> {
    let mut stmt = conn.prepare_cached("SELECT url FROM faces WHERE id == $1 LIMIT 1")
        .unwrap();

    let just_id = if id.ends_with(".jpg") {
        &id[..id.len() - 4]
    } else {
        &id
    };

    let mut rows = try!(stmt.query(&[&just_id.to_string()]));
    if let Some(result_row) = rows.next() {
        let row = try!(result_row);
        let url = row.get(0);

        let mut mod_url: String = url;
        mod_url.push_str("&h=128&w=128");

        Ok(Some(mod_url))
    } else {
        Ok(None)
    }
}

fn image_handler(req: &mut Request) -> IronResult<Response> {
    // grab database connection
    let pool = req.get::<Read<DbPool>>().expect("couldn't find database pool handle").clone();
    let conn = pool.get().expect("couldn't get db conn");

    // extract image hash url param from req
    let ref hash = iexpect!(req.extensions.get::<Router>().unwrap().find("image"));

    // lookup hash in db, redirect if found
    if let Some(url) = get_image_url(&conn, *hash).unwrap() {
        Ok(Response::with((status::SeeOther, Header(headers::Location(url)))))
    } else {
        Ok(Response::with((status::NotFound,
                           Header(headers::ContentType::plaintext()),
                           "Not Found")))
    }
}

fn root_handler(_req: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Ok, Header(headers::ContentType::plaintext()), "What up?")))
}

pub fn build_router() -> Router {
    let mut router = Router::new();
    router.get("/", root_handler, "root")
        .get("/:image", image_handler, "image");

    router
}
