use iron::prelude::*;
use iron::typemap::Key;
use persistent::Read;
use mount::Mount;
use logger::Logger;
use std::net::ToSocketAddrs;

use r2d2::Pool;
use r2d2_sqlite::SqliteConnectionManager;

use api;
use image_redirect;

#[derive(Copy, Clone)]
pub struct DbPool;

impl Key for DbPool {
    type Value = Pool<SqliteConnectionManager>;
}

pub fn build_router(pool: Pool<SqliteConnectionManager>) -> Chain {
    let (logger_before, logger_after) = Logger::new(None);

    debug!("building router");

    let mut mount = Mount::new();
    mount.mount("/v1", api::build_router())
        .mount("/", image_redirect::build_router());

    let mut chain = Chain::new(mount);
    chain.link(Read::<DbPool>::both(pool));

    // Link logger_before as first before middleware.
    chain.link_before(logger_before);

    // Link logger_after as *last* after middleware.
    chain.link_after(logger_after);

    chain
}

pub fn run<A: ToSocketAddrs>(addr: A, pool: Pool<SqliteConnectionManager>) {
    Iron::new(build_router(pool)).http(addr).unwrap();
}
